package com.nicedev.gtts.audio;

/**
 * Facade interface for stopping different players
 */
public interface StoppableAudioPlayer {
	void stop();
}
